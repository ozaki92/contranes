﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEye : MonoBehaviour {

	private int i = 0;
	public AudioClip explosion, win;
	[SerializeField]
	private GameObject parent, map;
	public GameObject ex, ex1, ex2, ex3, ex4, ex5, ex6;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "heroBullet") {
			i++;
			parent.GetComponent<AudioSource>().Play ();
		}
		if (i == 15) {
			parent.GetComponent<Animator>().SetBool ("ex", true);
			ex.GetComponent<Animator>().SetBool("Ex", true);
			ex1.GetComponent<Animator>().SetBool("Ex", true);
			ex2.GetComponent<Animator>().SetBool("Ex", true);
			ex3.GetComponent<Animator>().SetBool("Ex", true);
			ex4.GetComponent<Animator>().SetBool("Ex", true);
			ex5.GetComponent<Animator>().SetBool("Ex", true);
			ex6.GetComponent<Animator>().SetBool("Ex", true);
			parent.GetComponent<BoxCollider2D> ().enabled = false;
			parent.GetComponent<AudioSource>().clip = explosion;
			parent.GetComponent<AudioSource>().Play ();
			StartCoroutine (Ex ());
		}
	}

	IEnumerator Ex()
	{
		yield return new WaitForSeconds(3.0f);
		ex.GetComponent<Animator>().SetBool("Ex", false);
		ex1.GetComponent<Animator>().SetBool("Ex", false);
		ex2.GetComponent<Animator>().SetBool("Ex", false);
		ex3.GetComponent<Animator>().SetBool("Ex", false);
		ex4.GetComponent<Animator>().SetBool("Ex", false);
		ex5.GetComponent<Animator>().SetBool("Ex", false);
		ex6.GetComponent<Animator>().SetBool("Ex", false);
		parent.GetComponent<AudioSource>().clip = null;
		map.GetComponent<AudioSource> ().clip = win;
		map.GetComponent<AudioSource> ().loop = false;
		map.GetComponent<AudioSource> ().Play ();
	}
}
