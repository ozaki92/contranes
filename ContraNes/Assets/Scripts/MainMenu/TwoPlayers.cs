﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPlayers : MonoBehaviour {


    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (Manager.instance != null)
        {
            if (Manager.instance.bt2)
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            }
            else
            {
                this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }

}
