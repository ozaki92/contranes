﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

    public static Manager instance;
    public bool bt1, bt2;
    public bool onLoad = true;
    public int numberPlayer = 0;

    // Use this for initialization
    void Start () {
        _makeInstance();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void _makeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void _Button1PlayerClick()
    {
        bt1 = true;
        bt2 = false;
        numberPlayer = 1;
        PlayerPrefs.SetInt(KeySave.numberPlayerPlaying, numberPlayer);
        PlayerPrefs.Save();
    }

    public void _Button2PlayersClick()
    {
        bt2 = true;
        bt1 = false;
        onLoad = false;
        numberPlayer = 2;
        PlayerPrefs.SetInt(KeySave.numberPlayerPlaying, numberPlayer);
        PlayerPrefs.Save();
    }

    public void _ButtonPlayClick()
    {
        //Application.LoadLevel("Level1");
        int i = 1;
        PlayerPrefs.SetInt(KeySave.status, i);
        PlayerPrefs.SetInt(KeySave.score1, 0);
        PlayerPrefs.SetInt(KeySave.score2, 0);
        PlayerPrefs.Save();
        StartCoroutine(_wait());
    }

    IEnumerator _wait()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("Score");
    }
}
