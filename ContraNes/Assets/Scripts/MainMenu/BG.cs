﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG : MonoBehaviour {

    public float smoothing;
    public bool check;
    private bool playSound = false;

    public GameObject PlayButton;

    [SerializeField]
    private AudioSource AudioSrc;

    public static BG instance;

    // Use this for initialization
	void Start () {
        _makeInstance();
    }
	
	// Update is called once per frame
	void Update () {
        //transform.position = Vector3.Lerp(transform.position, new Vector3(0f, 0f, 0f), smoothing * Time.deltaTime);
        if (transform.position.x > 0f)
        {
            transform.position += Vector3.left * smoothing * Time.deltaTime;
        }
        if (transform.position.x <= 0)
        {
            check = true;
        }
        if(check == true && playSound == false)
        {
            AudioSrc.Play();
            playSound = true;
            PlayButton.active = true;
        }
    }

    void _makeInstance()
    {
        if(instance==null)
        {
            instance = this;
        }
    }
}
