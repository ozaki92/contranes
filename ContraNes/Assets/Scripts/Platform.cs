﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Platform : MonoBehaviour {

    [SerializeField]
    private Animator myAnim;
    private GameObject btnJump;
    private bool fall;
    // Use this for initialization
    void Start () {
        btnJump = GameObject.Find("JumpButton");
        btnJump.gameObject.GetComponent<Button>().onClick.AddListener(_fall);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            //if (myAnim.GetFloat("down") < 0.0f && myAnim.GetFloat("Speed") == 0.0f)
            if (collision.gameObject.GetComponent<Animator>().GetFloat("down") < 0.0f && collision.gameObject.GetComponent<Animator>().GetFloat("Speed") == 0.0f)
            {
                if (Input.GetKeyDown(KeyCode.Period) || fall)
                {
                    fall = false;
                    gameObject.GetComponent<Collider2D>().enabled = false;
                }
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Invoke("_Restore", 0.2f);
        }
    }

    void _Restore()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }

    public void _fall()
    {
        fall = true;
    }
}
