﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connon1 : MonoBehaviour {

	private float x,y;
	public Transform Player;
	[SerializeField]
	private Animator CannonAnim;

	private AudioSource audioSrc;
	public AudioClip pic;

	//Cac bien de ban
	public GameObject CannonBullet;
	private float nextFire = 0.0f;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		x = Player.transform.position.x - this.gameObject.transform.position.x;
		CannonAnim.SetFloat ("x", x);
		y = Player.transform.position.y - this.gameObject.transform.position.y;
		//y = this.gameObject.transform.position.y - Player.transform.position.y;
		CannonAnim.SetFloat ("y", y);
		if (y > 0) {
			CannonAnim.SetBool ("flip", true);
		} else {
			CannonAnim.SetBool ("flip", false);
		}
		if (x < 0) {
			CannonAnim.SetBool ("left", true);
		} else {
			CannonAnim.SetBool ("left", false);
		}
	}
		
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag=="Player")
		{
			StartCoroutine (_fire ());
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			StopAllCoroutines ();
		}
	}

	IEnumerator _fire()
	{
		_cannonFire ();
		yield return new WaitForSeconds (1f);
		StartCoroutine (_fire ());
	}
		
	void _cannonFire()
	{
		if (Time.time > nextFire) {
			nextFire = Time.time + 0.8f;
			if (x < -5.0f || (x < 0 && y > -0.5f && y < 0.5f)) {
				Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 180)));//9h
			}
			if (x > 5.0f || (x > 0 && y > -0.5f && y < 0.5f)) {
				Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 0)));//3h
			}
			if (y > 0) {
				if (x > -5.0f && x < -3.0f && y > 0.5f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 150)));//10h
				}
				if (x > -3.0f && x < -1.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 120)));//11h
				}
				if (x > -1.0f && x < 1.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 90)));//12h
				}
				if (x > 1.0f && x < 3.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 60)));//1h
				}
				if (x > 3.0f && x < 5.0f && y > 0.5f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 30)));//2h
				}
			} else {
				if (x > -5.0f && x < -3.0f && y < -0.5f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 210)));//8h
				}
				if (x > -3.0f && x < -1.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 240)));//7h
				}
				if (x > -1.0f && x < 1.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 270)));//6h
				}
				if (x > 1.0f && x < 3.0f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 300)));//5h
				}
				if (x > 3.0f && x < 5.0f && y < -0.5f) {
					Instantiate (CannonBullet, this.transform.position, Quaternion.Euler (new Vector3 (0, 0, 330)));//4h
				}
			}
		}
	}
}

