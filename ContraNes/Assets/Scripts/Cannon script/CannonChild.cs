﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonChild : MonoBehaviour {

	private int i=0;
	[SerializeField]
	private GameObject parentCannon, ex;
	public AudioClip exposion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "heroBullet") {
			i++;
			this.gameObject.GetComponentInParent<AudioSource> ().Play ();
		}
		if (i == 3) {
			parentCannon.GetComponent<AudioSource> ().clip = exposion;
			this.gameObject.GetComponentInParent<AudioSource> ().Play ();
			ex.GetComponent<Animator> ().SetBool ("Ex", true);
			StartCoroutine (Wait ());
		}
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds (0.8f);
		ex.GetComponent<Animator> ().SetBool ("Ex", false);
		Destroy(parentCannon);
	}
		
}
