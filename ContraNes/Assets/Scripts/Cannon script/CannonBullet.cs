﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : MonoBehaviour {

	public float bulletSpeed;
	[SerializeField]
	private Rigidbody2D bulletBody;
	public float aliveTime;

	void Awake ()
	{
		if (transform.localRotation.eulerAngles.z == 0)
		{
			bulletBody.AddForce(new Vector2(1, 0) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z == 180)
		{
			bulletBody.AddForce(new Vector2(-1, 0) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z == 90)
		{
			bulletBody.AddForce(new Vector2(0, 1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z == 270)
		{
			bulletBody.AddForce(new Vector2(0, -1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 0 && transform.localRotation.eulerAngles.z < 60)
		{
			bulletBody.AddForce(new Vector2(1, 0.05f) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 30 && transform.localRotation.eulerAngles.z < 90)
		{
			bulletBody.AddForce(new Vector2(0.577f, 1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 90 && transform.localRotation.eulerAngles.z < 150)
		{
			bulletBody.AddForce(new Vector2(-0.05f, 1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 120 && transform.localRotation.eulerAngles.z < 180)
		{
			bulletBody.AddForce(new Vector2(-1, 0.577f) * bulletSpeed, ForceMode2D.Impulse);
		}

		if (transform.localRotation.eulerAngles.z > 180 && transform.localRotation.eulerAngles.z < 240)
		{
			bulletBody.AddForce(new Vector2(-1, -0.577f) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 210 && transform.localRotation.eulerAngles.z < 270)
		{
			bulletBody.AddForce(new Vector2(-0.577f, -1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 270 && transform.localRotation.eulerAngles.z < 330)
		{
			bulletBody.AddForce(new Vector2(0.577f, -1) * bulletSpeed, ForceMode2D.Impulse);
		}
		if (transform.localRotation.eulerAngles.z > 300 && transform.localRotation.eulerAngles.z < 360)
		{
			bulletBody.AddForce(new Vector2(1, -0.577f) * bulletSpeed, ForceMode2D.Impulse);
		}
	}
	// Use this for initialization
	void Start () {
		Destroy(gameObject, aliveTime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			Destroy (this.gameObject);
		}
	}

}
