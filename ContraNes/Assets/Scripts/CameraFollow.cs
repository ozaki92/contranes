﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    private Transform player;
    public float minX, maxX, minTemp, smoothing;
    // Use this for initialization
	void Start () {
        
        minTemp = minX;
	}
	
	// Update is called once per frame
	void Update () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (player != null)
        {
            Vector3 temp = transform.position;
            temp.x = player.position.x;

            if (temp.x < minTemp)
            {
                temp.x = minTemp;
            }
            if (temp.x > maxX)
            {
                temp.x = maxX;
            }
            transform.position = Vector3.Lerp(transform.position, temp, smoothing * Time.deltaTime);
            //minTemp = transform.position.x;
        }
	}
}
