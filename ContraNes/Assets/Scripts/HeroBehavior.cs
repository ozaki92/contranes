﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroBehavior : MonoBehaviour {

    public float xAsis = 0, yAsis = 0;
    private GameObject joystick;
    private bool fire, jump;
    public float speed;
    [SerializeField]
    private Rigidbody2D mybody;
    [SerializeField]
    private Animator myAnim;
    private bool RightFace;
    private bool grounded;
    public float jumpHeight;
    public bool died, alive;

	//sound
	public AudioClip fireClip, diedClip, GetBullet;
	[SerializeField]
	private AudioSource audioSrc;
    //Cac bien de ban
    public GameObject bullet;
    public Transform gunTip;
    private float fireRate = 0.2f;
    private float nextFire = 0.0f;

    private GameObject btnFire, btnJump;

    public Sprite MBullet, defaultBullet;

    public static HeroBehavior instance;
	// Use this for initialization
	void Start () {
        RightFace = true;
        alive = true;
		audioSrc.clip = fireClip;
        instance = this;
        joystick = GameObject.Find("VirtualJoystick");
        btnJump = GameObject.Find("JumpButton");
        btnFire = GameObject.Find("FireButton");
        btnJump.gameObject.GetComponent<Button>().onClick.AddListener(_jump);
        btnFire.gameObject.GetComponent<Button>().onClick.AddListener(_fire);
    }
	
	// Update is called once per frame
	void Update () {
        if (myAnim.GetBool("inWater"))
        {
            myAnim.SetBool("swim", true);
        }

		if (alive) {
			//Chuc nang ban
			if (Input.GetKeyDown(KeyCode.Comma) || fire)
			{
				if (myAnim.GetFloat("Speed") < 0.01f)
				{
					if (myAnim.GetFloat("up") == 0)
					{
						if (myAnim.GetBool("swim"))
						{
							myAnim.SetBool("swimFire", true);
						}
						myAnim.SetBool("idleFire", true);
					}
					if (myAnim.GetFloat("up") > 0.01f)
					{
						if (myAnim.GetBool("swim"))
						{
							myAnim.SetBool("swimUpFire", true);
						}
						myAnim.SetBool("upFire", true);
					}
					if (myAnim.GetFloat("down") < -0.01f)
					{
						myAnim.SetBool("downFire", true);
					}
				}
				else
				{
					if (myAnim.GetFloat("up") == 0)
					{
						if (myAnim.GetBool("swim"))
						{
							myAnim.SetBool("swimFire", true);
						}
						myAnim.SetBool("runFire", true);
					}
					if (myAnim.GetFloat("up") > 0.01f && myAnim.GetBool("swim"))
					{
						myAnim.SetBool("swimSlantFire", true);
					}
				}
				_fireBullet();
                audioSrc.clip = fireClip;
                audioSrc.Play ();
                fire = false;
			}
			else
			{
				myAnim.SetBool("idleFire", false);
				myAnim.SetBool("upFire", false);
				myAnim.SetBool("downFire", false);
				myAnim.SetBool("runFire", false);
				myAnim.SetBool("swimUpFire", false);
				myAnim.SetBool("swimSlantFire", false);
				myAnim.SetBool("swimFire", false);
			}
		}
	}

    void FixedUpdate ()
    {
//#if UNITY_ANDROID

        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.x < -0.4f)
        {
            xAsis = -1;
        }
        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.x > -0.4f && joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.x < 0.4f)
        {
            xAsis = 0;
        }
        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.x > 0.4f)
        {
            xAsis = 1;
        }

        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.z < -0.4f)
        {
            yAsis = -1;
        }
        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.z > -0.4f && joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.z < 0.4f)
        {
            yAsis = 0;
        }
        if (joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection.z > 0.4f)
        {
            yAsis = 1;
        }

//#else
        if(joystick.gameObject.GetComponent<VirtualJoystick>().InputDirection == new Vector3(0,0,0))
        {
            xAsis = Input.GetAxisRaw("Horizontal");
            yAsis = Input.GetAxisRaw("Vertical");
        }

//#endif
        //float xAsis = Input.GetAxisRaw("Horizontal");
        //float yAsis = Input.GetAxisRaw("Vertical");

        if (alive)
        {
            if (myAnim.GetBool("swim") && myAnim.GetFloat("down") < -0.01f)
            {
                mybody.velocity = new Vector2(0, 0);
            }
            else
            {
                mybody.velocity = new Vector2(xAsis * speed, mybody.velocity.y);
            }
            myAnim.SetFloat("Speed", Mathf.Abs(xAsis));
            myAnim.SetFloat("down", yAsis);
            myAnim.SetFloat("up", yAsis);
            if (xAsis < 0.0f && RightFace)
            {
                flip();
            }
            else if (xAsis > 0.0f && !RightFace)
            {
                flip();
            }

            if (Input.GetKeyDown(KeyCode.Period) || jump)
            {
                jump = false;
                if (grounded && myAnim.GetBool("inWater") == false)
                {
                    if (myAnim.GetFloat("down") >= 0.0f || myAnim.GetFloat("Speed") > 0.01f)
                    {
                        mybody.AddForce(Vector2.up * jumpHeight);
                        myAnim.SetBool("jump", true);
                        grounded = false;
                    }
                }
            }
            if (mybody.velocity.y < -1.0f && myAnim.GetBool("jump") == false)
            {
                myAnim.SetBool("fall", true);
                grounded = false;
            }


        }
        
        if (myAnim.GetBool("died") == true && died)
        {
			if (alive) {
				audioSrc.clip = diedClip;
				audioSrc.Play ();
                alive = false;
                mybody.AddForce(Vector2.up * 300.0f);
                myAnim.SetBool("fall", false);
                StartCoroutine(DestroyHero(this.gameObject));
            }
			
            //died = false;
        }
    }

    public void _fire()
    {
        fire = true;
    }

    public void _jump()
    {
        jump = true;
    }

    void flip()
    {
        RightFace = !RightFace;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "ground" && mybody.velocity.y > -0.02f && mybody.velocity.y < 1.0f)
        {
            grounded = true;
            myAnim.SetBool("jump", false);
            myAnim.SetBool("fall", false);
        }
        if (other.gameObject.tag == "water")
        {
            myAnim.SetBool("inWater", true);
            myAnim.SetBool("fall", false);
        }
        else
        {
            myAnim.SetBool("inWater", false);
            myAnim.SetBool("swim", false);
        }
        if (other.gameObject.tag == "stone")
        {
            myAnim.SetBool("died", true);
            died = true;
            //other.gameObject.tag = "Untagged";
            bullet.gameObject.GetComponent<SpriteRenderer>().sprite = defaultBullet;
            bullet.gameObject.GetComponent<BulletControler>().bulletSpeed = 10f;
        }

        if (other.gameObject.tag == "MBullet")
        {
            audioSrc.clip = GetBullet;
            audioSrc.Play();
            bullet.gameObject.GetComponent<SpriteRenderer>().sprite = MBullet;
            bullet.gameObject.GetComponent<BulletControler>().bulletSpeed = 12f;
        }
    }

    void _fireBullet ()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (RightFace)//quay mat ben phai
            {
                if (myAnim.GetBool("swimFire") == true)//boi ban
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x - 0.2f, gunTip.position.y - 0.85f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 0)));
                }
                if (myAnim.GetBool("swimUpFire") == true)//boi ban len troi
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x - 0.5f, gunTip.position.y, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));
                }
                if (myAnim.GetBool("swimSlantFire") == true)//boi ban cheo
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x - 0.15f, gunTip.position.y - 0.2f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 45)));
                }
                if (myAnim.GetFloat("Speed") == 0.00f && myAnim.GetBool("swim") == false)
                {
                    if (myAnim.GetFloat("up") == 0.0f)//dung yen ban
                    {
                        Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 0)));
                    }
                    if (myAnim.GetFloat("up") < 0.0f && myAnim.GetBool("jump") == false)//nam ban
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x, gunTip.position.y - 0.67f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 0)));
                    }
                    if (myAnim.GetFloat("up") > 0.01f)//ban len troi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x - 0.55f, gunTip.position.y + 0.85f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));
                    }
                    if (myAnim.GetBool("jump") == true && myAnim.GetFloat("down") < 0.0f)//nhay ban xuong duoi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x - 0.65f, gunTip.position.y - 0.55f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 270)));
                    }
                }
                if (myAnim.GetFloat("Speed") > 0.00f && myAnim.GetBool("swim") == false)
                {
                    if (myAnim.GetFloat("up") == 0.0f)//chay ban
                    {
                        Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 0)));
                    }
                    if (myAnim.GetFloat("up") > 0.01f)//ban cheo len tren
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x - 0.25f, gunTip.position.y + 0.5f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 45)));
                    }
                    if (myAnim.GetFloat("up") < -0.01f && myAnim.GetBool("swim") == false)//ban cheo xuong duoi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x - 0.25f, gunTip.position.y - 0.3f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 315)));
                    }
                }
            }
            else//quay mat ben trai
            {
                if (myAnim.GetBool("swimFire") == true)//boi ban
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x + 0.2f, gunTip.position.y - 0.85f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 180)));
                }
                if (myAnim.GetBool("swimUpFire") == true)//boi ban len troi
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x + 0.5f, gunTip.position.y, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));
                }
                if (myAnim.GetBool("swimSlantFire") == true)//boi ban cheo
                {
                    Instantiate(bullet, new Vector3(gunTip.position.x + 0.15f, gunTip.position.y - 0.2f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 135)));
                }
                if (myAnim.GetFloat("Speed") == 0.00f && myAnim.GetBool("swim") == false)
                {
                    if (myAnim.GetFloat("up") == 0.0f)//dung yen ban
                    {
                        Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 180)));
                    }
                    if (myAnim.GetFloat("up") < 0.0f && myAnim.GetBool("jump") == false)//nam ban
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x, gunTip.position.y - 0.67f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 180)));
                    }
                    if (myAnim.GetFloat("up") > 0.01f)//ban len troi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x + 0.55f, gunTip.position.y + 0.85f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));
                    }
                    if (myAnim.GetBool("jump") == true && myAnim.GetFloat("down") < 0.0f)//nhay ban xuong duoi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x + 0.65f, gunTip.position.y - 0.55f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 270)));
                    }
                }
                if (myAnim.GetFloat("Speed") > 0.00f && myAnim.GetBool("swim") == false)
                {
                    if (myAnim.GetFloat("up") == 0.0f)//chay ban
                    {
                        Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 180)));
                    }
                    if (myAnim.GetFloat("up") > 0.01f)//ban cheo len tren
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x + 0.25f, gunTip.position.y + 0.5f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 135)));
                    }
                    if (myAnim.GetFloat("up") < -0.01f && myAnim.GetBool("swim") == false)//ban cheo xuong duoi
                    {
                        Instantiate(bullet, new Vector3(gunTip.position.x + 0.25f, gunTip.position.y - 0.3f, gunTip.position.z), Quaternion.Euler(new Vector3(0, 0, 225)));
                    }
                }
            }
        }
    }

    //va cham voi enemy
    void OnTriggerEnter2D(Collider2D other)
    {
		if (myAnim.GetCurrentAnimatorStateInfo (0).IsName ("Dive") == false)
        {
			if (other.gameObject.tag=="enemy" || other.gameObject.tag == "enemyBullet")
			{
				died = true;
				myAnim.SetBool("died", true);
                bullet.gameObject.GetComponent<SpriteRenderer>().sprite = defaultBullet;
                bullet.gameObject.GetComponent<BulletControler>().bulletSpeed = 10f;
            }
		}

        if (other.gameObject.tag == "MBullet")
        {
            audioSrc.clip = GetBullet;
            audioSrc.Play();
            bullet.gameObject.GetComponent<SpriteRenderer>().sprite = MBullet;
            bullet.gameObject.GetComponent<BulletControler>().bulletSpeed = 12f;
        }
    }

    IEnumerator DestroyHero(GameObject gameObject)
    {
        yield return new WaitForSeconds(1.0f);
        Destroy(gameObject);
    }
}
