﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraBullet : MonoBehaviour {

    public GameObject ex;
    public AudioSource audioSrc;
    public AudioClip exposion;
    public GameObject Box, MBullet;
    public Rigidbody2D rgbBullet;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "heroBullet")
        {
            audioSrc.Play();
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            ex.GetComponent<Animator>().SetBool("Ex", true);
            rgbBullet.gravityScale = 1;
            rgbBullet.AddForce(new Vector2(100f, 500f));
            StartCoroutine(Wait());
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.7f);
        ex.GetComponent<Animator>().SetBool("Ex", false);
        Box.SetActive(false);
    }
}
