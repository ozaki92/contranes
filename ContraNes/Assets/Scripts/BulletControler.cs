﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControler : MonoBehaviour {

    public float bulletSpeed;
    [SerializeField]
    private Rigidbody2D myBody;
    public float aliveTime;

    private int score;

	// Use this for initialization
    void Awake ()
    {
        if (transform.localRotation.eulerAngles.z == 0)
        {
            myBody.AddForce(new Vector2(1, 0) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z == 180)
        {
            myBody.AddForce(new Vector2(-1, 0) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z == 90)
        {
            myBody.AddForce(new Vector2(0, 1) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z == 270)
        {
            myBody.AddForce(new Vector2(0, -1) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z > 0 && transform.localRotation.eulerAngles.z < 90)
        {
            myBody.AddForce(new Vector2(1, 1) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z == 315)
        {
            myBody.AddForce(new Vector2(0.8f, -0.8f) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z == 135)
        {
            myBody.AddForce(new Vector2(-0.8f, 0.8f) * bulletSpeed, ForceMode2D.Impulse);
        }
        if (transform.localRotation.eulerAngles.z > 180 && transform.localRotation.eulerAngles.z < 270)
        {
            myBody.AddForce(new Vector2(-0.8f, -0.8f) * bulletSpeed, ForceMode2D.Impulse);
        }
    }

	void Start () {
        score = PlayerPrefs.GetInt(KeySave.score1, 0);
        Destroy(gameObject, aliveTime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.tag == "enemy" || other.gameObject.tag == "boss")
        {
            score += 100;
            PlayerPrefs.SetInt(KeySave.score1, score);
            PlayerPrefs.Save();
            Destroy(gameObject);
        }
        //if(other.gameObject.tag=="ExtraBullet")
        //{
        //    Destroy(gameObject);
        //}
    }
}
