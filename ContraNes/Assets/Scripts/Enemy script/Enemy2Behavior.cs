﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Behavior : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D enemyRB;
    [SerializeField]
    private Animator enemyAM;
    public bool died = false;

    private float x, y;
    private Transform Player;

    [SerializeField]
	private AudioSource audioSrc;

    //Cac bien de ban
    public GameObject CannonBullet;
    private float nextFire = 0.0f;

    public static Enemy2Behavior instance;

    // Use this for initialization
    void Start () {
        _makeInstance();
	}

    void Update()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        x = Player.transform.position.x - this.gameObject.transform.position.x;
        enemyAM.SetFloat("x", x);
        y = Player.transform.position.y - this.gameObject.transform.position.y;
        enemyAM.SetFloat("y", y);

        if (y > 0)
        {
            enemyAM.SetBool("flip", true);
        }
        else
        {
            enemyAM.SetBool("flip", false);
        }
        if (x < 0)
        {
            enemyAM.SetBool("left", true);
        }
        else
        {
            enemyAM.SetBool("left", false);
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (died)
        {
            enemyAM.SetBool("died", true);
            enemyRB.velocity = new Vector2(1.7f, 1.7f);
            Destroy(this.gameObject, 0.7f);
        }
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(_fire());
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StopAllCoroutines();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "enemy")
        {
            this.gameObject.GetComponent<CircleCollider2D>().isTrigger = true;
            //enemyRB.constraints = RigidbodyConstraints2D.None;
            //enemyRB.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    public void _enemy2Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + 0.8f;
            if (x < -5.0f || (x < 0 && y > -0.5f && y < 0.5f))
            {
                Instantiate(CannonBullet, new Vector3(this.transform.position.x, this.transform.position.y + 0.35f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 180)));//9h
            }
            if (x > 5.0f || (x > 0 && y > -0.5f && y < 0.5f))
            {
                Instantiate(CannonBullet, new Vector3(this.transform.position.x, this.transform.position.y + 0.35f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 0)));//3h
            }
            if (y > 0)
            {
                if (x > -5.0f && x < -3.0f && y > 0.5f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 150)));//10h
                }
                if (x > -3.0f && x < -1.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 120)));//11h
                }
                if (x > -1.0f && x < 0.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));//12h
                }
                if (x > 0.0f && x < 1.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 90)));//12h
                }
                if (x > 1.0f && x < 3.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 60)));//1h
                }
                if (x > 3.0f && x < 5.0f && y > 0.5f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.35f, this.transform.position.y + 0.8f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 30)));//2h
                }
            }
            else
            {
                if (x > -5.0f && x < -3.0f && y < -0.5f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 210)));//8h
                }
                if (x > -3.0f && x < -1.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 240)));//7h
                }
                if (x > -1.0f && x < 0.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x - 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 270)));//6h
                }
                if (x > 0.0f && x < 1.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 270)));//6h
                }
                if (x > 1.0f && x < 3.0f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 300)));//5h
                }
                if (x > 3.0f && x < 5.0f && y < -0.5f)
                {
                    Instantiate(CannonBullet, new Vector3(this.transform.position.x + 0.36f, this.transform.position.y - 0.25f, this.transform.position.z), Quaternion.Euler(new Vector3(0, 0, 330)));//4h
                }
            }
        }
    }

    IEnumerator _fire()
    {
        _enemy2Fire();
        yield return new WaitForSeconds(0.8f);
        StartCoroutine(_fire());
    }

    void _makeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
}
