﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Child : MonoBehaviour {

    [SerializeField]
    private Enemy2Behavior parent;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "heroBullet")
        {
            if (Enemy2Behavior.instance != null)
            {
                parent.died = true;
                parent.GetComponent<AudioSource>().Play();
            }
        }
    }
}
