﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour {

    private Transform camera;

    // Use this for initialization
	void Start () {
        camera = GameObject.Find("Main Camera").transform;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 temp = camera.position;
        temp.x = camera.position.x - 7.0f;
        transform.position = temp;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "enemy")
        {
            Destroy(other.gameObject);
        }
    }
}
