﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Moverment : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D enemyRB;
    [SerializeField]
    private Animator enemyAM;
    private bool died;

	[SerializeField]
	private AudioSource audioSrc;
    public bool run;

    // Use this for initialization
	void Start () {

	}
    private void Update()
    {
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (died == false)
        {
            if (run)
            {
                enemyRB.velocity = new Vector2(-2.5f, 0f);
            }
            else
            {
                enemyRB.velocity = new Vector2(-0.4f, 0f);
                enemyRB.gravityScale = 12;
            }
        }
        
        if (died == true)
        {
            enemyAM.SetBool("died", true);
            this.gameObject.tag = "Untagged";
            //enemyRB.AddForce(Vector2.up * 30.0f);
            enemyRB.velocity = new Vector2(1.5f, 1.5f);
            Destroy(this.gameObject, 0.6f);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag=="heroBullet")
        {
            died = true;
			audioSrc.Play ();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "stone" || other.gameObject.tag == "water")
        {
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "ground")
        {
            run = true;
            enemyRB.gravityScale = 7;
        }
    }
    public void OnCollisionStay2D(Collision2D other)
    {
        run = true;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "ground")
        {
            run = false;
            enemyRB.gravityScale = 0;
            StartCoroutine(Wait());
        }
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.25f);
        enemyRB.gravityScale = 7;
    }
}
