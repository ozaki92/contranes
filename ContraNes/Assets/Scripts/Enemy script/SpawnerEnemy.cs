﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemy : MonoBehaviour {

    [SerializeField]
    private GameObject enemy;
	//private GameObject player;
    private Transform camera;
    // Use this for initialization
	void Start () {
        camera = GameObject.Find("Main Camera").transform;
		//player = GameObject.Find ("Hero").transform;
        StartCoroutine(Spawner());
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 temp = transform.position;
        temp.x = camera.position.x + 12.0f;
        temp.y = camera.position.y + 5f;
        transform.position = temp;
	}

    IEnumerator Spawner()
    {
		yield return new WaitForSeconds(Random.Range(2f, 4f));
		Instantiate(enemy, transform.position, Quaternion.identity);
		StartCoroutine (Spawner());
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StopAllCoroutines();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(Spawner());
        }
    }
}
