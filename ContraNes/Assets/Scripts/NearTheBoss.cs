﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearTheBoss : MonoBehaviour {

	[SerializeField]
	private GameObject SpawnEnemy;
	[SerializeField]
	private Transform player;
	private int i = 0;
	[SerializeField]
	private AudioSource audioSrc;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (player != null)
        {
            if (player.transform.position.x > 137.7f && i < 3)
            {
                i++;
            }
            if (i == 1)
            {
                audioSrc.Play();
            }
        }
        
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			SpawnEnemy.SetActive (false);
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
		}
	}
}
