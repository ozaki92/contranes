﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge32 : MonoBehaviour {

	[SerializeField]
	private GameObject ex, ex1, ex2, ex3;
	[SerializeField]
	private Animator BrAnim;
	public bool start;
	public static Bridge32 instance;

	//sound
	[SerializeField]
	private AudioSource audioSrc;
	public AudioClip Explorde;

	// Use this for initialization
	void Start () {
		MakeInstance();
	}

	// Update is called once per frame
	void Update () {
		if (Bridge22.instance != null)
		{
			if (Bridge22.instance.start)
			{
				StartCoroutine(Ex());
				Bridge22.instance.start = false;
			}
		}
	}

	IEnumerator Ex()
	{
		yield return new WaitForSeconds(0.9f);
		BrAnim.SetBool("Ex1", true);
		start = true;
		this.gameObject.GetComponent<Collider2D>().enabled = false;
		audioSrc.Play ();
		ex.GetComponent<Animator>().SetBool("Ex", true);
		ex1.GetComponent<Animator>().SetBool("Ex", true);
		ex2.GetComponent<Animator>().SetBool("Ex", true);
		ex3.GetComponent<Animator>().SetBool("Ex", true);
		StartCoroutine(off());
	}
	IEnumerator off()
	{
		yield return new WaitForSeconds(0.7f);
		ex.GetComponent<Animator>().SetBool("Ex", false);
		ex1.GetComponent<Animator>().SetBool("Ex", false);
		ex2.GetComponent<Animator>().SetBool("Ex", false);
		ex3.GetComponent<Animator>().SetBool("Ex", false);
		BrAnim.SetBool("Ex1", false);
	}

	void MakeInstance()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
}
