﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge12 : MonoBehaviour {

	[SerializeField]
	private Animator BrAnim;
	[SerializeField]
	private GameObject ex, ex1, ex2, ex3;
	public BoxCollider2D box1, box2;
	public bool start;
	public static Bridge12 instance;

	//sound
	[SerializeField]
	private AudioSource audioSrc;
	public AudioClip Explorde;

	// Use this for initialization
	void Start () {
		MakeInstance();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			BrAnim.SetBool("Ex1", true);
			start = true;
			box1.enabled = false;
			box2.enabled = false;
			audioSrc.Play ();
			ex.GetComponent<Animator>().SetBool("Ex", true);
			ex1.GetComponent<Animator>().SetBool("Ex", true);
			ex2.GetComponent<Animator>().SetBool("Ex", true);
			ex3.GetComponent<Animator>().SetBool("Ex", true);
			StartCoroutine(Ex());
		}
	}

	IEnumerator Ex()
	{
		yield return new WaitForSeconds(0.7f);
		ex.GetComponent<Animator>().SetBool("Ex", false);
		ex1.GetComponent<Animator>().SetBool("Ex", false);
		ex2.GetComponent<Animator>().SetBool("Ex", false);
		ex3.GetComponent<Animator>().SetBool("Ex", false);
		BrAnim.SetBool("Ex1", false);
	}

	void MakeInstance()
	{
		if (instance == null)
		{
			instance = this;
		}
	}
}
