﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerScoreScene : MonoBehaviour {

    public GameObject PanelNameStage;
    public GameObject PanelGameOver;
    public Text txtScore1;
    public Text txtScore2;
    public Text txtHighScore;
    public Text txtRest1;
    public Text txtRest2;
    public Text txtNameStage;

    private int status = 0;
    //public bool gameOver;
    //public bool next;
    private int level;
    public int rest = 0;
    public int numPlayer;
    public AudioSource audioSrc;
    public AudioClip gameOver;

    // Use this for initialization
    void Start () {
        status = PlayerPrefs.GetInt(KeySave.status);

        int i = PlayerPrefs.GetInt(KeySave.score1, 0);
        int j = PlayerPrefs.GetInt(KeySave.score2, 0);
        int k = PlayerPrefs.GetInt(KeySave.highScore);
        if (i > j)
        {
            if (i > k)
            {
                PlayerPrefs.SetInt(KeySave.highScore, i);
            }
        }
        else
        {
            if (j > k)
            {
                PlayerPrefs.SetInt(KeySave.highScore, j);
            }
        }

        if (status == 1)
        {
            PanelNameStage.SetActive(true);
            PanelGameOver.SetActive(false);
            rest = 3;
            numPlayer = PlayerPrefs.GetInt(KeySave.numberPlayerPlaying);
            if (numPlayer == 2)
            {
                PlayerPrefs.SetInt(KeySave.rest1, rest);
                PlayerPrefs.SetInt(KeySave.rest2, rest);
                PlayerPrefs.Save();
            }
            else
            {
                PlayerPrefs.SetInt(KeySave.rest1, rest);
                PlayerPrefs.SetInt(KeySave.rest2, 0);
                PlayerPrefs.Save();
            }
            switch (level)
            {
                case 1:
                    txtNameStage.text = "STAGE 1 \n" + "JUNGLE";
                    break;
                case 2:
                    txtNameStage.text = "STAGE 2\n" +
                        "ABC";
                    break;
                default:
                    break;
            }

            StartCoroutine(_play());
        }

        if (status == 2)
        {
            PanelNameStage.SetActive(false);
            PanelGameOver.SetActive(true);
            audioSrc.PlayOneShot(gameOver);
            //Time.timeScale = 0f;
        }

        txtScore1.text = PlayerPrefs.GetInt(KeySave.score1, 0).ToString();
        txtScore2.text = PlayerPrefs.GetInt(KeySave.score2, 0).ToString();
        txtHighScore.text = "HI " + PlayerPrefs.GetInt(KeySave.highScore, 0).ToString();
        txtRest1.text = PlayerPrefs.GetInt(KeySave.rest1, 0).ToString();
        txtRest2.text = PlayerPrefs.GetInt(KeySave.rest2, 0).ToString();
    }

    public void BackMenu()
    {
        StartCoroutine(_wait());
    }

    IEnumerator _wait()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene("MainMenu");
    }

    public void Continue()
    {
        int i = 1;
        PlayerPrefs.SetInt(KeySave.status, i);
        Start();
    }

    IEnumerator _play()
    {
        yield return new WaitForSeconds(2.0f);
        PlayerPrefs.SetInt(KeySave.score1, 0);
        PlayerPrefs.SetInt(KeySave.score2, 0);
        PlayerPrefs.Save();
        SceneManager.LoadScene("Level1");
    }

    // Update is called once per frame
    void Update () {
        
    }
}
