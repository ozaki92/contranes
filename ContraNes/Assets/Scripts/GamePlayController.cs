﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayController : MonoBehaviour {

    private Transform camera;

    public GameObject hero;
    public int rest = 0;
    public bool playing, onetime;
    public GameObject restItem1, restItem2, restItem3, restItem4, gameOverItem;
    public AudioSource audioSrc;
    public AudioClip bonusRest;
    private bool isBonusRest, isGetBonus;
    private int score, scoreBonus;

    public static GamePlayController instance;
    // Use this for initialization
    void Start () {
        //DontDestroyOnLoad(this.gameObject);
        instance = this;
        camera = GameObject.Find("Main Camera").transform;
        rest = PlayerPrefs.GetInt(KeySave.rest1);
        if (rest >= 1)
        {
            playing = false;
            onetime = true;
            StartCoroutine(Spaw());
        }
        
    }
    IEnumerator Spaw()
    {
        yield return new WaitForSeconds(0.7f);
        Instantiate(hero, transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 temp = transform.position;
        temp.x = camera.position.x - 6.3f;
        temp.y = camera.position.y + 6.5f;
        transform.position = temp;

        score = PlayerPrefs.GetInt(KeySave.score1);

        if (rest < 1)
        {
            playing = false;
            int i = 2;
            PlayerPrefs.SetInt(KeySave.status, i);
            gameOverItem.SetActive(true);
            StartCoroutine(_wait());
        }
        else
        {
            playing = true;
        }
        if (HeroBehavior.instance.died == true && onetime == false && playing == true)
        {
            onetime = true;
            if (rest > 1)
            {
                StartCoroutine(Revival());
            }
            rest = rest - 1;
            PlayerPrefs.SetInt(KeySave.rest1, rest);
            PlayerPrefs.Save();
        }
        if (HeroBehavior.instance.died == false)
        {
            onetime = false;
        }

        if (rest >= 4)
        {
            restItem4.SetActive(true);
        }
        else
        {
            restItem4.SetActive(false);
        }

        if (rest >= 3)
        {
            restItem3.SetActive(true);
        }
        else
        {
            restItem3.SetActive(false);
        }

        if (rest >= 2)
        {
            restItem2.SetActive(true);
        }
        else
        {
            restItem2.SetActive(false);
        }

        if (rest >= 1)
        {
            restItem1.SetActive(true);
        }
        else
        {
            restItem1.SetActive(false);
        }

        if (score > 0 && score % (10000) == 0)
        {
            isBonusRest = true;
            if (score > scoreBonus)
            {
                isGetBonus = true;
            }
            scoreBonus = score;
        }
        if(isBonusRest && isGetBonus)
        {
            isGetBonus = false;
            rest = rest + 1;
            PlayerPrefs.SetInt(KeySave.rest1, rest);
            PlayerPrefs.Save();
            audioSrc.PlayOneShot(bonusRest);
        }
    }

    IEnumerator Revival()
    {
        yield return new WaitForSeconds(1.5f);
        Instantiate(hero, transform.position, Quaternion.identity);
    }

    IEnumerator _wait()
    {
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("Score");
    }
}
