﻿

public class KeySave {

    public const string score1 = "score1";
    public const string score2 = "score2";
    public const string highScore = "highScore";
    public const string rest1 = "rest1";
    public const string rest2 = "rest2";
    public const string numberPlayerPlaying = "numberPlayerPlaying";
    public const string status = "status";
    public const string isWin = "isWin";

}
